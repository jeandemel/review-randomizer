let btnPick = document.querySelector('#pick');
let btnReset = document.querySelector('#reset');
let p = document.querySelector('#volunteer');

const students = ["aaougabi","fcolonnello","vrossirojas","bmunoz","mhavetissian","jdudon","mfernandez","mbatier","ajoachim","abobichon","bhuguenin","dregazzoni","femerit","fligonnet","jchabert","jbazan","jkraft","nperret","pgeoffroy","rzegar","asakhri","mlamotte","amoreira","gbonne"];
shuffle(students);

let done = localStorage.getItem('done');
if(done && done <= 23) {
    done = JSON.parse(done);
} else {
    done = [];
}

btnPick.addEventListener('click', event => {
    for(let student of students) {        
        if(done.indexOf(student) === -1) {
            console.log(student);
            done.push(student);
            localStorage.setItem('done', JSON.stringify(done));
            p.textContent = student;
            break;
        }
    }    
});

btnReset.addEventListener('click', event => {
    done = [];
    localStorage.setItem('done', JSON.stringify(done));
});

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}