fetch('reviewers.json').then(data => {
    const target = document.querySelector('.reviewers');
    data.json().then(reviewers => {
        for (let reviewer in reviewers) {
            target.innerHTML += `<div class="dropdown col-3">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            ${reviewer}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">${reviewers[reviewer][0]}</a>
            <a class="dropdown-item" href="#">${reviewers[reviewer][1]}</a>
        </div>
    </div>`;

        }
    });
});