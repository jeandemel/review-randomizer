const students = ["aaougabi","fcolonnello","vrossirojas","bmunoz","mhavetissian","jdudon","mfernandez","mbatier","ajoachim","abobichon","bhuguenin","dregazzoni","femerit","fligonnet","jchabert","jbazan","jkraft","nperret","pgeoffroy","rzegar","asakhri","mlamotte","amoreira","gbonne"];
const shuffled = Array.from(students);
let toReview = {};
let reviewedBy = {};
shuffle(shuffled);

console.log(shuffled);


for(let student of students) {
    toReview[student] = [];
    for(let reviewed of shuffled) {
        if(!reviewedBy.hasOwnProperty(reviewed)) {
            reviewedBy[reviewed] = 0;
        }
        if(reviewedBy[reviewed] > 1) {
            continue;
        }
        if(reviewed !== student && !toReview[student].includes(reviewed)) {
                toReview[student].push(reviewed);
                reviewedBy[reviewed]++;
                if(toReview[student].length > 1) {
                    
                    break;
                }
        }
    }
    shuffle(shuffled);
}
let b64 = btoa(JSON.stringify(toReview));
document.querySelector('a').href = "data:application/octet-stream;charset=utf-8;base64,"+b64;

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}